import React, { Component } from 'react';
import './App.css';
import {BrowserRouter, Route, Switch} from "react-router-dom";
import HomePage from "./containers/HomePage/HomePage";
import AddedQuote from "./containers/AddedQuote/AddedQuote";

class App extends Component {
  render() {
    return (
      <div className="App">

          <BrowserRouter>
              <Switch>
                  <Route path="/quotes/:categoryId" component={HomePage}/>
                  <Route path="/" exact component={HomePage}/>
                  <Route path="/add-quote" component={AddedQuote}/>
              </Switch>
          </BrowserRouter>
      </div>
    );
  }
}

export default App;
