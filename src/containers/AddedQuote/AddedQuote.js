import React, {Component, Fragment} from 'react';
import AddQuoteForm from "../../components/AddQuoteForm/AddQuoteForm";
import axios from '../../axios-quotes'

class AddedQuote extends Component {

    addQuote = quote => {
        axios.post('quotes.json', quote).then(() => {
            this.props.history.replace('/');
        });
    };

    render() {
        return (
            <Fragment>
                <AddQuoteForm onSubmit={this.addQuote}/>
            </Fragment>
        );
    }
}

export default AddedQuote;