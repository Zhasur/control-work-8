import React, {Component, Fragment} from 'react';
import Header from "../../components/Header/Header";
import {NavLink} from "react-router-dom";
import axios from '../../axios-quotes'
import {CATEGORIES} from '../../constants'

import './HomePage.css'

class HomePage extends Component {
    state = {
        quotes: null,
    };

    loadData() {
        let url = 'quotes.json';

        const categoryId = this.props.match.params.categoryId;
        if (categoryId) {
            url += "?orderBy=category&equalTo=" + categoryId
        }

        axios.get(url).then(response => {
            const quotes = Object.keys(response.data).map(id => {
                return {...response.data[id], id}
            });

            this.setState({quotes});

        }).catch(error => {
            return error;
        });
    }

    componentDidMount() {
        this.loadData();
    };

    componentDidUpdate(prevProps) {
        if (this.props.match.params.categoryId
            && this.props.match.params.categoryId !== prevProps.match.params.categoryId) {
            this.loadData();
        }
    }

    deleteQuote = () => {

        axios.delete('quotes/' + this.props.match.params.id).then(() => {
            this.props.history.replace('/');

        });

    };


    render() {
        let quotes = null;

        if (this.state.quotes) {
            quotes = this.state.quotes.map((quote, key) => (
                    <div className="inner-quotes" key={key}>
                        <p className='text'>{quote.quote.textQuote}</p>
                        <span className="author">{quote.quote.author}</span>
                        <div className="make-change">
                            <button className="edit"><i className="far fa-edit" /></button>
                            <button onClick={this.deleteQuote} className="delete-btn">x</button>
                        </div>
                    </div>
                )
            )
        }
        return (
            <Fragment>
                <Header/>
                <div className="main-block">
                    <div className="quotes-list">
                        <ul className="main-list">
                            <NavLink className="list" to='/' exact>All</NavLink>
                            {Object.keys(CATEGORIES).map(categoryId => (
                                <NavLink className="list" key={categoryId} to={'/quotes/' + categoryId}>
                                    {CATEGORIES[categoryId]}
                                </NavLink>
                            ))}
                        </ul>
                    </div>
                    <div className="quotes-block">
                        <h2 className="title-category">All</h2>
                        {quotes}
                    </div>

                </div>
            </Fragment>
        );
    }
}

export default HomePage;