import React, {Component, Fragment} from 'react';
import Header from "../Header/Header";

import './AddQuoteForm.css'
import {CATEGORIES} from "../../constants";

class AddQuoteForm extends Component {

    state = {
        author: '',
        textQuote: '',
        category: Object.keys(CATEGORIES)[0]
    };

    valueChanged = event => {
        this.setState({[event.target.name]: event.target.value})
    };

    quoteHandler = (event) => {

        event.preventDefault();

        const quotes = {
            quote: {
                author: this.state.author,
                textQuote: this.state.textQuote,
                category: this.state.category
            }
        };

        this.props.onSubmit(quotes)

    };

    render() {
        return (
            <Fragment>
                <Header/>
                <div className="submit-block">
                    <h2>Submit new quote</h2>
                    <form onSubmit={this.quoteHandler} className="form">
                        <div className="category-form">
                            <label style={{display: 'block', margin: '10px'}}>Category</label>
                            <select name="category" className="category-options"
                                    value={this.state.category}
                                    onChange={this.valueChanged}
                            >
                                {Object.keys(CATEGORIES).map(categoryId => (
                                    <option key={categoryId} value={categoryId}>{CATEGORIES[categoryId]}</option>
                                ))}
                            </select>
                        </div>
                        <div className="author-form">
                            <label style={{display: 'block', margin: '10px'}}>Author</label>
                            <input type="text" name="author" className="author"
                                   onChange={this.valueChanged}
                                   value={this.state.author}
                            />
                        </div>
                        <div className="quote-text-form">
                            <label style={{display: 'block', margin: '10px'}}>Quote text</label>
                            <input type="text" name="textQuote" className="textQuote"
                                   onChange={this.valueChanged}
                                   value={this.state.textQuote}
                            />
                        </div>
                        <button type="submit" className="save-btn">Send</button>
                    </form>
                </div>
            </Fragment>
        );
    }
}

export default AddQuoteForm;