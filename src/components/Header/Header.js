import React from 'react';
import {NavLink} from "react-router-dom";

import './Header.css'

const Header = () => {
    return (
        <header className="header">
            <NavLink className="nav-link" to="/">Quotes Central</NavLink>
            <ul className="main-nav">
                <NavLink className="nav-link quotes" to="/">Quotes</NavLink>
                <NavLink className="nav-link" to="/add-quote">Submit new quote</NavLink>
            </ul>
        </header>
    );
};

export default Header;