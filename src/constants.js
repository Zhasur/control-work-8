export const CATEGORIES = {
    'StarWars': 'Star Wars',
    'FamousPeople': 'Famous People',
    'Saying': 'Saying',
    'Humor': 'Humor',
    'Motivational': 'Motivational',
};